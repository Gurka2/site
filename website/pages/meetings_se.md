# Stormöten

Två gånger om året, en gång på våren och en gång på hösten, hålls ett stormöte i
enlighet med stadgarna. På höstmötet gås föregående verksamhetsår igenom med en
verksamhetsberättelse, en ekonomisk berättelse och revisorns granskning av
styrelsens arbete följt av en röstning om ansvarsfrihet. På vårmötet röstas
nästa verksamhetsårs styrelse in. På både höst- och vårmöten kan motioner lyftas
av medlemmar och propositioner av styrelsen.

Tidigare protokoll finns i [arkivet på vår Gitlab](https://gitlab.com/lithekod/stormoten).

## Höstmöte 2023

Nästa stormöte är höstmötet 2023 som hålls den 21 november 18.15 i Ada Lovelace (ingång B27).
Samtliga medlemmar får närvara med rösträtt.
Nedan följer mötets preliminära föredrangingslista.

- Val av mötesordförande
- Val av mötessekreterare
- Val av justeringsperson tillika rösträknare
- Fastställande av röstlängden
- Beslut om mötets stadgeenliga utlysande
- Föregånde verksamhetsårs styrelses verksamhetsberättelse
- Föregånde verksamhetsårs styrelses ekomomiska berättelse
- Revisorns granskning av föregående verksamhetsårs styrelses arbete
- Beslut om ansvarsfrihet av föregående verksamhetsårs styrelse
- Motioner och propositioner
- Övriga frågor

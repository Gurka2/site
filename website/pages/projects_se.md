Projekt är för dig som är medlem i LiTHe kod och vill anordna egen verksamhet,
men inte som en del av våra undergrupper. Ett exempel skulle vara en kodstuga,
en workshop, en föreläsning eller någon annan typ av evenemang.

Vi har [ett intresseformulär]() du kan fylla i om du är intresserad av att dra
igång ett projekt. Det är inte ett bindande formulär men det är bra för att
starta en diskussion med styrelsen.

## Aktiva/kommande projekt

*Inga just nu*

## Gamla projekt

Vi har än så länge inte haft några medlemsprojekt, men om vi får igång något ser
vi till att lägga en lista här.

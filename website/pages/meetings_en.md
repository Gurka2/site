# "Stormöten"

Twice a year, once in spring and once in fall, a "stormöte" is held in
accordance with the bylaws. During the fall meeting the previous financial year
is walked through by presenting the previous board's report
("verksamhetsberättelse"), financial report ("ekonomiska berättelse") and the
auditor's review followed by a decision on discharge from liability ("beslut om
ansvarsfrihet"). During the spring meeting the next financial year's board is
voted on.

Since the meetings are held in Swedish, information about the next meeting can
be found on the Swedish version of this page.

Old protocols (in Swedish) can be found in the [archive on our Gitlab](https://gitlab.com/lithekod/stormoten).

## Fall meeting 2023

The "stormöte" will be held Tuesday November 21 at 6:15 pm in Ada Lovelace.
All members may participate and vote.
Below is the agenda for the meeting.

- Election of meeting chairman
- Election of meeting secretary
- Choice of adjuster as well as counters
- Determining the voting list
- Resolution on the meeting's statutory announcement
- Previous boards' report of the previous financial year
- Financial report of the board for the previous financial year
- The auditor's review of the previous financial year's board work
- Decision on discharge from liability of the board of the previous financial year
- Motions and bills
- Other questions

# Hacktoberfest

Hacktoberfest is a global event held every October by Digital Ocean.
Participants are encouraged to submit 4 merge requests (or pull requests) to
participating repositories in order to take their first steps into open source.
Read more about Hacktoberfest on
[hacktobrefest.com](https://hacktoberfest.com/).

In 2021 we held a presentation about Hacktoberfest which you can view below. If
you have any questions you are very welcome to join us on our meetups, every
Tuesday at 17:15 in Café Java.

- [Hacktoberfest 2021, by Frans Skarman](/static/hacktoberfest-2021.pdf)

This year we've decided to create a list of projects that members of LiTHe kod
are active in, so you can try out open source and easily ask questions! The
projects are summarized in a table, and described in more detail further down.

| Projekt | Kort beskrivning | Språk | Kontakt (Discord) |
|---|---|---|---|
| <hr> | <hr> | <hr> | <hr> |
| [lithekod.se](https://gitlab.com/lithekod/www/site) | lithekod.se | Python | |
| [Kodapan](https://gitlab.com/lithekod/kodapa-2) | Styrelens Discord-bot | Rust | |
| <hr> | <hr> | <hr> | <hr> |
| [Spade](https://spade-lang.org/) | Hårdvaruprogrammeringsspråk | Rust | Frans Skarman (@thezoq2) |
| [Sputnik](https://gitlab.com/sputnik-engine/sputnik) | Spelmotor till Lua | Rust | Gustav Sörnäs (@sornas) |

If you are a member of LiTHe kod and want to add your own projects here you can
either open a merge request against [LiTHe kod's
website](https://gitlab.com/lithekod/www/site) (remember to edit both the
Swedish and English page) or send a message to someone in the board who can add
it for you. Make sure to add a hacktoberfest-tag to your repository and create
some tickets for stuff that someone who is interested in helping out can get
started with.

## Project o' the board

These projects are maintained by the board and are used by LiTHe kod in some way.

### lithekod.se

[GitLab](https://gitlab.com/lithekod/www/site)

The website you're on right now! If you see something you want to change, feel
free to open an issue to see if it's something the board is interested in.

### Kodapan

[GitLab](https://gitlab.com/lithekod/kodapa-2)

Kodapan is styrelsens Discord-bot that keeps track of meeting agendas and
reminders for board meetings. The board would like the bot to remind about
meetups in the announcement channel. There are also a few internal things to
work on, for example the communication with the Google Calendar API.

## Medlemmars projekt

These projects are maintained by members. Feel free to ask questions in the
Discord server or during one of our meetups!

### Spade

[GitLab](https://gitlab.com/spade-lang), [spade-lang.org](https://spade-lang.org)

Spade is a programming language for FPGAs and other programmable hardware. With
programming you can help with the compiler and our own build system, but if you
have an FPGA at home (that can be programmed with an open source toolchain) you
could try to add a template configuration.

### Sputnik

[GitLab](https://gitlab.com/sputnik-engine/sputnik)

Sputnik is a very new game engine written in Rust. It compiles to a Lua module
which means that it can be used both in Lua and in other languages that either
compile to Lua or that can understand and import Lua modules. There is a lot of
things to do, but I'll make sure that there are some small and easy functions to
implement. You can also write small demo games.

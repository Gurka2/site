<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam Workshop</h1>
</div>

Vi i LiU Game Jam anordnar workshoppar för att ge nya game jam deltagare en
introduktion i game jam-spelskapandet! Under en kväll kommer vi gå igenom
typiska bra saker att ha koll på under ett game jam.

## Vad händer på workshoppen?

Vi gick över tre centrala delar av spelskapandet:

- Brainstorming,
- Arts och Graphics,
- Spelprogrammering i spelmotorn Godot

## Om jag inte närvarade någon av dem?

Inga problem, att närvara vid en workshop är inget obligatoriskt,
det är till för lite extra hjälp. Nedan har vi laddat upp presnetationen 
och de två spelen vi skapade tillsammans med deras källkod.

- [Presentaionsmaterialet](/static/gamejamworkshop.pdf) (samma för båda workshoppsen)

## Workshop I

Under första workshoppen skapade vi ett spel där ett tåg flyter till och från 
månen för att hämta ost, eftersom månen är gjort av ost. Spelet kallades Space Train 3:
In Hope of Cheese och spelaren konmtrollerar tåget med 'A' och 'D' för att rotera 
vänster och höger.

### Spelet

Det finns ingen restart-knapp. Du måste ladda om sidan.

[Space Train 3: In Hope of Cheese](/static/hope-of-cheese/index.html)

[Källkod på GitLab](https://gitlab.com/lithekod/liu-game-jam/2023-workshop-session-1).

## Workshop II

Under vår andra workshop gjorde vi ett spel där ett tåg hoppas mellan
tre spår för att undvika hinder och döda alienbarn; som man gör. Spelet kallades för
Yawbus och även denna gång användes 'A' och 'D' som kontroller för att hoppa vänster och höger.

### The game

Även här finns ingen restart-knapp. Du måste ladda om sidan.

[Yawbus](/static/yawbus/yawbus.html)

[Source code also on GitLab](https://gitlab.com/lithekod/liu-game-jam/2023-workshop-session-2).

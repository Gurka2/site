<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam Workshop</h1>
</div>

We at LiU Game Jam will organize workshops to give new game jam participants an
introduction to game jam game creation! During one evening we will go through
typical good things to keep track of during your first game jam.

## Content

We went over three central parts of game making:

- Brainstorming,
- Arts and Graphics,
- Game programming in the game engine Godot

## Didn't attend any of them?

Don't worry, being present at one of the workshops is not mandatory 
to attend a game jam, it is just a help. Below we have uploaded the
presentation and the two games we created together with their source code.

- [Presentation material](/static/gamejamworkshop.pdf) (same material on both workshops)

## Workshop I

During the first workshop, we made a game where a train flies to and from the
moon to collect cheese, since the moon is made of cheese. The game was called
Space Train 3: In Hope of Cheese and the player controls the train with 'A' and
'D' to rotate left and right.

### The game

There is no restart. You will have to reload the page to try again.

[Space Train 3: In Hope of Cheese](/static/hope-of-cheese/index.html)

[Source code on GitLab](https://gitlab.com/lithekod/liu-game-jam/2023-workshop-session-1).

## Workshop II

During the second workshop, we made a game where a train jumps between 
three tracks to dodge obsticles and kill alien babies; as you do.
The game was called yawbus and this time we also used 'A' and 'D' to
jump left and right.

### The game

Once again there is no restart. You will have to reload the page to try again.

[Yawbus](/static/yawbus/yawbus.html)

[Source code also on GitLab](https://gitlab.com/lithekod/liu-game-jam/2023-workshop-session-2).

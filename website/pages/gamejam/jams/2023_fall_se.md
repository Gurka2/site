<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam</h1>
</div>

[⇦ Tillbaka till huvudsidan för LiU Game Jam](/gamejam/)

# Fall Game Jam 2023

*17-19 November*

<div id=important-information>
<p>
Medan du väntar, <a href="https://discord.gg/tP2kDvgQKn">kom å häng i vår Discord-server</a>!
</p>
</div>

<img src="/static/img/gamejam/banner-fgj23.png" alt="Fall Game Jam 2023" id="gamejam-banner">

Gör dig redo för årets Fall Game Jam! Häng med och bygg grymma spel
tillsammans under en helg. Återigen fysiskt på plats!

### Workshop

Är du intresserad av spelutveckling, men det verkar alldeles för stort, svårt eller avancerat? Var inte rädd!
Vår game jam-workshop är öppet för alla, oavsett tidigare erfarenhet, och lär dig det du behöver för att kunna
skapa ditt egna spel under vårt game jam.

Workshoppen hålls två gånger, 9:e och 14:e november med starttid 17:30 och förväntas hålla på i ungefär 2 timmar.
Vi kommer gå igenom art, speldesign och hur man gör ett första spel i spelmotorn Godot. 


<!--
### Information under jammet
-->

### Lokal

Game-jammet kommer vara på [Spektrum, Ebbepark](https://sanktkors.se/lediga-lokaler/linkoping/ebbepark/spektrum/) i Linköping. Lokalen har Wi-Fi men inga fysiska nätverksuttag.

### Delta

För att delta på evenemanget: kom till Spektrum klockan 18:00 (eller lite tidigare) fredag den 17 november. Ta med det du behöver för att bygga spel; laptop eller fysiska spelkomponenter.

Valfritt:

0. Gå med på [Itch.io-sidan](https://itch.io/jam/liu-fall-game-jam-2023) där där du gärna får ladda upp resultatet så att det blir mer tillgängligt och kan läggas till i vårt spelakriv:
[Itch.io-arkiv](https://itch.io/c/64050/liu-game-jam).
0. Gå med i [vår Discord-server](https://discord.gg/tP2kDvgQKn) för att få ny information under jammet.
0. Gå med i
[Facebook-evenemanget](https://www.facebook.com/events/1077978989857126).
<br/>*Det hjälper oss att uppskatta hur många som kommer delta, för att kunna planera jammet bättre.*

### Schema

**Fredag 17 november**

- 18:00 Introduktion
    - Temareveal
    - Brainstorming
    - Gruppskapande
- 18:10 Gemensam pizza-beställning (deadline)

**Lördag 18 november**

- 09:00 Spektrum öppnar
    - Frukost (gratis)
- 16:00 Gemensam kinabuffé-beställning (deadline)
- Runt 18:00: Kinabuffé serveras
- Runt 18:15: Halvtidsredovisning

**Söndag 19 november**

- 09:00 Spektrum öppnar
    - Frukost (gratis)
- 11:00 Gemensam sushi-beställning (deadline)
- 18:00 Game Jam-deadline
    - Spelen ska vara uppladdade till globalgamejam.org och itch.io
    - Deltagare ska ha städat sina bord och sin omgivning
- 18:15: Slutredovisning
- 19:00: Speltestning och uppvisning

### Tema

Teman för eventet skapas under introduktionen genom att para ihop ord som lämnats in av deltagarna.

Regler för tema-ord:

- Ska bara vara ett enda engelskt ord.
- Ska inte vara en spelgenre (ex. FPS, RTS).
- Undvik osedligt innehåll. (Ska vara safe for work.)

Vår temaprocess skapar unika och utmanande teman varje jam. De finns som inspiration under brainstormsessionen, men du kan bygga efter egna idéer också.

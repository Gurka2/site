<div id="gamejam-header">
  <img src="/static/img/gamejam/logo.png" alt="LiU Game Jam">
  <h1>LiU Game Jam</h1>
</div>

[⇦ Back to LiU Game Jam's main page](/gamejam/)

# Spring Game Jam 2023

*18:00 12th May - 18:00 14th May*

<div id=important-information>
<p>
While you're waiting, come <a href="https://discord.gg/tP2kDvgQKn">hang out in our Discord server</a>!
</p>
</div>

<img src="/static/img/gamejam/banner-sgj23.png" alt="Spring Game Jam 2023" id="gamejam-banner">

Get ready for this year's edition of Spring Game Jam! Join us and build kickass
games together during a weekend. Once again IN REAL LIFE!

<!--
### Information during the jam
-->

### Location

The game jam will take place on site at [Spektrum,
Ebbepark](https://sanktkors.se/lediga-lokaler/linkoping/ebbepark/spektrum/) in
Linköping, Sweden. The location has Wi-Fi but no physical network outlets.

### How to participate

In order to participate in the jam, show up at Spektrum at (or a bit before)
18:00 12th May. Bring the equipment you need in order to create games:
laptop and peripherals, or physical game components.

Also, optionally:

0. Join the [itch.io jam page](https://itch.io/jam/liu-spring-game-jam-2023) where we would like for you to upload
   the resulting game too, so it is more accesible and can be added to our game
   archive:
   [Itch.io Archive](https://itch.io/c/64050/liu-game-jam)
0. Join our Discord to get announcements during the jam:
   [Discord invite](https://discord.gg/tP2kDvgQKn)
0. Join the [Facebook event](https://www.facebook.com/events/786567499340170)<br/>*This helps us
   approximate the number of attending jammers for better planning.*

### Schedule

**Friday 12th May**

- 18:00: Introduction
    - Theme reveal
    - Brainstorming
    - Group forming
- 18:10: Joint pizza order deadline

**Saturday 13th May**

- 09:00: Spektrum opens
    - Breakfast (free)
- 16:00: Joint Chinese buffet order deadline
- Around 18:00: Chinese food arrival and buffet
- Around 18:15: Half time review

**Sunday 14th May**

- 09:00: Spektrum opens
    - Breakfast (free)
- 11:00: Joint sushi order deadline
- 18:00: Game creation deadline
    - Resulting games should be uploaded to globalgamejam.org and itch.io
    - Jammers should have cleaned their tables
- 18:15: Final review
- 19:00: Playtesting and showcase

### Theme

The themes are generated at the start of the jam by taking random pairs of words
submitted by participants. They will be added at the top of this page after the start of the jam.

Theme word rules:

- Should only be one English word.
- Should not be a game genre (e.g. FPS, RTS).
- Keep it safe for work.

Our theme process ensures unique and challenging themes each jam. The themes are
there for inspiration during brainstorming but you can pick something else as
well.

# NCPC - 2023

NCPC är en programmeringstävling för programmerare av alla erfarenhetsnivåer som kommer att infalla lördag den 7 oktober.
Att vara ny till programmering är inte ett problem!
Tävlingen sker i lag av upp till 3 personer och under tävlingen kommer du tillsammans med ditt lag lösa roliga och kluriga problem.
I mitten av tävlingen kommer vi bjuda alla deltagare på pizza 🍕 och några veckor efter tävlingen kommer du kunna hämta upp en gratis NCPC-T-shirt 👕!
Om man presterar tillräckligt bra har man möjlighet att åka vidare och tävla i nordvästra europa och sedan i världsmästerskapen, men glöm inte att det viktiga är att ha roligt!

## Jag vill vara med, vad gör jag?

1.  Hitta ett upp till två personer att tävla tillsammans med (om du inte vill tävla själv, vilket är helt
    okej!)
2.  Registrera dig själv och ditt lag på [ICPC:s hemsida](https://icpc.global/regionals/finder/Nordic), senast den 3 oktober.
3.  Fyll i vårt [deltagarformulär](https://forms.gle/Eo8iXf54AHe3LLFn8), särskilt om du vill få din gratis T-shirt!

## Detaljer

Dagen kommer att börja med en intropresentation kl. 10:00 i Ada Lovelace.
För att hitta till Ada Lovelace så går du in i B-huset, ingång 27, och går rakt framåt till Café Java.
Där kommer vi informera om tävlingen och våra sponsorer kommer få chansen att dela några ord.

Tävlingen börjar kl. 11:00 och sker i lag om upp till tre personer i universitets labbsalar.
Varje lag har tillgång till <ins>en</ins> dator, antingen en egen dator eller en av universitets (då är LiU-id är ett krav).
Under tävlingen kommer vi att dela ut ballonger till lag för varje löst problem och runt 13:00 så bjuder vi på pizza.
Tävlingen slutar 16:00.

När tävlingen är över kommer vi återsamlas i Ada Lovelace för en kort slutpresentation.

Det bästa eller de två bästa lagen kommer få möjlighet att åka vidare till Delft, Nederländerna, för att tävla i NWERC.
Tyvärr kommer vi inte kunna erbjuda en gratis resa om inte universitet går in med pengar.
Om man får möjligheten rekommenderar vi det starkt!
Det är ett evenemang där man träffar fantastiska människor och blir otroligt bra bemött.
Det blir en helg man sent glömmer!

Om ni har några frågor kan ni kontakta mig, @lowekoz på Discord, eller skicka ett mejl till vår ordförande Henry [ordf@lithekod.se](mailto:ordf@lithekod.se).

### Schema (7 oktober 2023)

-   10:00 - Intropresentation i Ada Lovelace (ingång 27 till B-huset)
-   11:00 - Tävlingen börjar!
-   Runt 13:00 - Gratis lunch
-   16:00 - Tävlingen avslutas
-   16:15 - Slutpresentation

### Regler

-   Lag består av upp till tre medlemmar som är svenska medborgare eller har en relation till Linköpings Universitet.
-   Tävlingen sker endast på plats.
-   Ditt lag får endast använda en dator under tävlingen (en mus, ett tangentbord och en skärm). Antingen en egen dator eller en dator i labbsalarna.
-   Ditt lag **FÅR INTE**:
    -   använda ytterligare elektroniska enheter för att tävla.
    -   programmera med hjälp av generativ AI (GitHub Copilot, ChatGPT, etc.).
    -   kommunicera med någon förutom arrangörerna eller dina lagmedlemmar.
-   Ditt lag **FÅR**:
    -   ta med utskrivet material (böcker, dokument, etc.).
    -   använda färdigskriven kod och annan mjukvara på din dator som inte bryter ovanstående regler (alltså ingen GitHub Copilot, ChatGPT, etc.).

En fullständig regeluppsättning hittar ni på [NCPCs hemsida](https://nordic.icpc.io/ncpc2023/compete#rules).

### Kvalifikationskriterier för NWERC

Framsteg till NWERC kan endast erbjudas lag som är [berättigade till
ICPC](https://icpc.global/regionals/rules), det vill säga om deltagarna:

-   Inte har deltagit i NCPC eller motsvarande fem olika år.
-   Inte har deltagit i ICPC två olika år.
-   Uppfyller minst ett av följande kraven:
    -   Började studera vid universitet eller jämförbart 2019 eller senare.
    -   Föddes år 2000 eller senare.

<div id="sponsor-container">
    <img class="sponsor only-dark-theme" src="/static/img/opera_dt.png" alt="opera">
    <img class="sponsor only-light-theme" src="/static/img/opera_lt.png" alt="opera">
    <img class="sponsor only-dark-theme" src="/static/img/axis_dt.png" alt="axis">
    <img class="sponsor only-light-theme" src="/static/img/axis_lt.jpg" alt="axis">
</div>

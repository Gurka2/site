# Competitions

LiTHe kod hosts several different programming competitions during the year.
Below you will find information about these events.

## Current competitions

- [IMPA](impa/) - All year!

## Upcoming competitions

- October 7, 2023: [NCPC](ncpc/2023/)
- December 2023: [Advent of Code (2023)](aoc/)

## Earlier competitions

### 2023

- [LiU Challenge (Swedish Coding Cup)](liu-challenge/)

### 2022

- Advent of Code
- [NCPC](ncpc/2022/)

### 2021

- Advent of Code
- [NCPC](ncpc/2021/)

### 2020

- Advent of Code
- [NCPC](ncpc/2020/)

### 2019

- Advent of Code
- [NCPC](ncpc/2019/)
- LiU Challenge (Swedish Coding Cup)

And probably a few more that we have forgotten.

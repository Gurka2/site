# Organisation

LiTHe kod är en fristående studentförening vid Linköpings universitet. Till
skillnad från sektionerna och deras undergrupper kan vem som helst vara medlem
och delta i LiTHe kods verksamhet.

Om du vill engagera dig i föreningen finns det några olika sätt beroende på vad
du är ute efter.

- Om du bara vill **gå på roliga evenemang** behöver du bara bli medlem, vilket
  du enklast gör genom att antingen ta kontakt med någon i styrelsen eller dyka
  upp på en av våra [meetups](/meetups/), tisdagar klockan 17 i Café Java.
- Om du vill **engagera dig i en undergrupp**, antingen för att anordna
  verksamhet eller för att hitta folk med liknande intressen, kan du läsa mer om
  om vilka som finns och hur du tar kontakt med dem på vår sida om [våra
  undergrupper](/groups/).
- Om du vill **anordna egen verksamhet** som du tror skulle passa föreningen kan
  du ansöka om att få starta upp ett projekt. Du kommer behöva beskriva
  projektet och eventuellt presentera ett budgetförslag till styrelsen. Du kan
  läsa mer om den här processen på vår sida om
  [projekt](/projects/).

Här hittar du dokument och information tillhörande organisationen.

## [Höst- och vårmöten](/meetings/)

## [Github](https://github.com/lithekod)

## [Stadgar](https://github.com/lithekod/bylaws/blob/master/stadgar.pdf)

## [Stormötesprotokoll](https://github.com/lithekod/stormoten)

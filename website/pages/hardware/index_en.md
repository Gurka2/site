# The hardware group

Welcome to LiTHe kod's hardware group! We are a subgroup to LiTHe kod who are
interested in hardware and other hardware-adjacent programming, like
programmable hardware, micro controllers and embedded system.

Since we're a pretty new subgroup we don't have any planned activities. But that
also means there are more possibilities to have a say! If you're interested in
what were up to, or just want to learn more, please get in touch. We are most
easily reached in the channel `#hårdvara` in [LiTHe kod's Discord server](https://discord.gg/UG5YYsN) or during LiTHe kod's meetups every Tuesday in Café Java.

Here's a list of things we're thinking about doing:

- Our own logo (fit for a 128x32 SSD1306 OLED, of course).
- RFID reader for opening the door to LiTHe kod's room.
- Pimp my fikavagn, with RGB.
- Build keyboards with LiTHe kod. We buy the stuff, you get help with putting it
  together and programming the firmware with QMK/ZMK.
- Our own arcade machine. We could ask M-verkstan for help with building a
  chassis. Outside we put some nice buttons (hot-swappable?) and inside some
  kind of control unit, either a full-scale computer or something with more
  retro-inspired limitations, like an Arduino or a custom soft core.

We also have a small collection of Arduino Uno, Raspberry Pi and external
components. We'll gladly help out if you want to try programming a smaller
computer than you're used to.
